# Gun Store - React App

[![pipeline status](https://gitlab.com/cc-full-stack/gun-store/front-end/badges/master/pipeline.svg)](https://gitlab.com/codegod/gun-store/commits/master)
[![test coverage](https://gitlab.com/cc-full-stack/gun-store/front-end/badges/master/coverage.svg?job=test)](https://gitlab.com/codegod/gun-store/commits/master)

## Demo
Demo available [here](https://gun-store-front-end.herokuapp.com/)

## Get started

```bash
 npm install
 npm start

or

 yarn && yarn start
```

## Testing

```bash
 yarn test
```

## Running dev server

```bash
 yarn dev
```
