import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import ShopWithRouter from './shop';
import { Locations } from './locations';
import './App.scss';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={ShopWithRouter} />
          <Route path="/locations" component={Locations} />
          <Redirect from="/*" to="/" />
        </Switch>
      </Router>
    );
  }
}

export default App;
