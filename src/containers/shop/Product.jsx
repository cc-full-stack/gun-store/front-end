import React, { Component } from 'react';
import { connect } from 'react-redux';
import { cartAddProduct } from '../../actions/storeActions';
import './Product.scss';

export class Product extends Component {
  constructor(props) {
    super(props);
    this.handleAddToCart = this.handleAddToCart.bind(this);
  }

  handleAddToCart() {
    this.props.addProduct(this.props.product);
  }

  render() {
    return (
      <div>
        <div className="uk-card uk-card-default uk-card-hover uk-card-body uk-flex uk-flex-column">
          <div className="uk-card-title">{this.props.product.title}</div>
          <a
            className="uk-inline product__media"
            href={this.props.product.image}
            caption={this.props.product.title}
          >
            <div className="uk-card-media-top">
              <img src={this.props.product.image} alt="" />
            </div>
          </a>
          <div className="uk-flex">
            <div className="uk-width-2-3">
              <button className="uk-button uk-button-secondary" onClick={this.handleAddToCart}>
                Add to cart
              </button>
            </div>
            <div className="uk-width-1-3 product__price">{this.props.product.price} $</div>
          </div>
        </div>
      </div>
    );
  }
}

export const cartDispatchToProps = dispatch => ({
  addProduct: (product) => {
    /* eslint-disable no-undef */
    UIkit.notification("<span uk-icon='icon: check'></span> Product added to cart.", {
      pos: 'top-right',
    });
    /* eslint-enable */
    return dispatch(cartAddProduct(product));
  },
});

export default connect(null, cartDispatchToProps)(Product);
