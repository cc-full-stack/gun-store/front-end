import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import ProductsConnected from './Products';
import CartConnected from './Cart';
import AddProductModalConnected from '../../components/AddProductModal';
import { StyledMapWithAnInfoBox } from '../map/map';

export class Shop extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange() {
    this.props.history.push('/locations');
  }

  render() {
    return (
      <div className="uk-container uk-padding">
        <nav uk-navbar>
          <div className="uk-navbar-right">
            <ul className="uk-navbar-nav">
              <li className="uk-active">
                <button
                  className="uk-button uk-button-default"
                  uk-toggle="target: #cart-off-canvas"
                >
                  <span uk-icon="icon: cart" />
                </button>
              </li>
              <li className="uk-active">
                <button
                  onClick={this.routeChange}
                  id="router-btn"
                  className="uk-button uk-button-default"
                >
                  Locations &nbsp;
                  <span uk-icon="icon: tag" />
                </button>
              </li>
            </ul>
          </div>
        </nav>
        <ProductsConnected products={this.props.products} />
        <CartConnected />
        <AddProductModalConnected />
        <p className="uk-text-large">Where you can find us?</p>
        <StyledMapWithAnInfoBox
          center={{ lat: 47.0105, lng: 28.8638 }}
          mapStyle="default"
          defaultLocation="true"
        />
      </div>
    );
  }
}

export default withRouter(Shop);
