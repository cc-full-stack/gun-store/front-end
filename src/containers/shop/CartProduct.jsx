import React, { Component } from 'react';
import { connect } from 'react-redux';
import { cartRemoveProduct, cartSetProductQTY } from '../../actions/storeActions';

export class CartProduct extends Component {
  constructor(props) {
    super(props);
    this.handleProductQtyChange = this.handleProductQtyChange.bind(this);
    this.handleCartProductDelete = this.handleCartProductDelete.bind(this);
  }

  handleProductQtyChange(e) {
    this.props.cartSetProductQTY({
      id: this.props.product.id,
      qty: e.target.value,
    });
  }

  handleCartProductDelete() {
    this.props.removeProduct(this.props.product);
  }

  render() {
    return (
      <article className="uk-comment">
        <header className="uk-comment-header uk-grid-medium uk-flex-middle" uk-grid>
          <div className="uk-width-auto">
            <img
              className="uk-comment-avatar"
              src={this.props.product.image}
              width="80"
              height="80"
              alt={this.props.product.title}
            />
          </div>
          <div className="uk-width-expand">
            <h4 className="uk-comment-title uk-margin-remove">
              <span className="uk-link-reset">{this.props.product.title}</span>
            </h4>
            <ul
              className="uk-comment-meta uk-subnav
              uk-subnav-divider uk-margin-removde-top cart__product_qty"
            >
              <li>
                <span>
                  {this.props.product.price} $ x{' '}
                  <input
                    min="1"
                    value={this.props.product.qty}
                    onChange={this.handleProductQtyChange}
                    type="number"
                    name="qty"
                  />
                </span>
              </li>
              <li>
                <button type="button" onClick={this.handleCartProductDelete} uk-close />
              </li>
            </ul>
          </div>
        </header>
      </article>
    );
  }
}

export const cartDispatchToProps = dispatch => ({
  removeProduct: product => dispatch(cartRemoveProduct(product)),
  cartSetProductQTY: product => dispatch(cartSetProductQTY(product)),
});

export default connect(null, cartDispatchToProps)(CartProduct);
