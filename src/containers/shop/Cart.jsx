import React, { Component } from 'react';
import { connect } from 'react-redux';
import CartProductConnected from './CartProduct';
import './Cart.scss';

export class Cart extends Component {
  render() {
    return (
      <div id="cart-off-canvas" uk-offcanvas>
        <div className="uk-offcanvas-bar cart">
          <button className="uk-offcanvas-close" type="button" uk-close />
          <h3>Cart</h3>
          {this.props.products.length !== 0 ? (
            <h6>
              Total:
              {this.props.products
                .map(product => product.price * product.qty)
                .reduce((a, b) => a + b, 0)}
              $
            </h6>
          ) : (
            ''
          )}
          {this.props.products.map(product => (
            <CartProductConnected key={product.id} product={product} />
          ))}
          {this.props.products.length !== 0 ? (
            ''
          ) : (
            <div className="cart__message">There is no items in cart</div>
          )}
        </div>
      </div>
    );
  }
}

export const cartProductsToProps = state => ({ products: state.reducers.store.cart });

export default connect(cartProductsToProps)(Cart);
