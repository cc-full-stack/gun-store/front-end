import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import ProductConnected from './Product';
import { storeSetProducts } from '../../actions/storeActions';
import './Products.scss';

export class Products extends Component {
  constructor(props) {
    super(props);
    this.loading = true;
    this.fetchProducts();
  }

  fetchProducts() {
    return axios
      .get('/products')
      .then((response) => {
        this.loading = false;
        return this.props.setProducts(response.data);
      })
      .catch((error) => {
        /* eslint-disable no-undef */
        UIkit.notification(
          { message: `<span uk-icon='icon: close'></span>${error.message}`, status: 'danger' },
          {
            pos: 'top-right',
          },
        );
        /* eslint-enable */
      });
  }

  render() {
    if (this.loading) {
      return (
        <div
          className="products uk-child-width-1-3@m uk-grid-match uk-margin-medium"
          uk-lightbox="animation: slide"
          uk-grid="true"
        >
          <div className="uk-flex uk-flex-column" />
          <div className="uk-flex uk-flex-column uk-flex-center">
            <span data-uk-spinner="" className="spin uk-flex" uk-spinner="ratio: 5" />
          </div>
          <div className="uk-flex uk-flex-column" />
        </div>
      );
    }
    return (
      <div
        className="products uk-child-width-1-3@m uk-grid-match uk-margin-medium"
        uk-lightbox="animation: slide"
        uk-grid="true"
      >
        {this.props.products.map(product => (
          <ProductConnected key={product.id} product={product} />
          ))}
        <div>
          <div
            className="uk-card uk-card-default uk-card-hover
             uk-card-body uk-flex uk-flex-center add_card"
            uk-toggle="target: #add-product-modal"
          >
            <span className="uk-margin-small-center add_icon" uk-icon="icon: plus; ratio: 10" />
          </div>
        </div>
      </div>
    );
  }
}

export const storeProductsToProps = state => ({
  products: state.reducers.store.products,
});

export const storeDispatchToProps = dispatch => ({
  setProducts: products => dispatch(storeSetProducts(products)),
});

export default connect(storeProductsToProps, storeDispatchToProps)(Products);
