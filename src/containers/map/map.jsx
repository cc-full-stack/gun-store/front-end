import React from 'react';
import { compose, withProps, withStateHandlers } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { InfoBox } from 'react-google-maps/lib/components/addons/InfoBox';
import mapStyle from './style.json';

/* eslint-disable no-undef */

export const initialState = {
  isOpen: true,
};

export const onToggleOpen = ({ isOpen }) => () => ({
  isOpen: !isOpen,
});

export const MapComponent = props => (
  <GoogleMap
    defaultZoom={5}
    defaultCenter={props.center}
    defaultOptions={{ styles: mapStyle[props.mapStyle] }}
  >
    {props.defaultLocation && (
      <Marker position={props.center} onClick={props.onToggleOpen}>
        {props.isOpen && (
          <InfoBox
            onCloseClick={props.onToggleOpen}
            options={{ closeBoxURL: '', enableEventPropagation: true }}
          >
            <div style={{ backgroundColor: '#dcdde1', opacity: 0.75, padding: '12px' }}>
              <div style={{ fontSize: '16px', fontColor: '#08233B' }}>
                Here we are, you thought in Kandahar? Nah!
              </div>
            </div>
          </InfoBox>
        )}
      </Marker>
    )}
    {props.locations &&
      props.locations.map(location => (
        <Marker
          key={location.coordinates.lat.toString() + location.coordinates.lon.toString()}
          position={{ lat: location.coordinates.lat, lng: location.coordinates.lon }}
        />
      ))}
  </GoogleMap>
);

export const StyledMapWithAnInfoBox = compose(
  withProps({
    googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${
      process.env.REACT_APP_GOOGLE_API_KEY
    }&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: '100%' }} />,
    containerElement: <div style={{ height: '700px' }} />,
    mapElement: <div style={{ height: '100%' }} />,
  }),
  withStateHandlers(initialState, { onToggleOpen }),
  withScriptjs,
  withGoogleMap,
)(MapComponent);
