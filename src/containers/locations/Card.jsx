import React, { Component } from 'react';
import './Card.scss';

export default class Card extends Component {
  // show growth/fall of a location
  displayRevenue() {
    const revenue = this.props.location.revenue < 0;
    return (
      <div className="revenue">
        <img
          src={`assets/img/${revenue ? 'right_down' : 'right_up'}.svg`}
          className={revenue ? 'fall' : 'growth'}
          alt={revenue ? 'fall' : 'growth'}
        />
        <p className={revenue ? 'red' : 'green'}>{this.props.location.revenue}%</p>
        <p>{revenue ? '*less than usual' : '*more than usual'}</p>
      </div>
    );
  }


  render() {
    const tooltipSettings = JSON.stringify({
      title: this.props.location.description,
      pos: 'bottom-right',
      offset: 15,
    });

    return (
      <div className="card uk-card uk-card-default uk-card-hover uk-card-body uk-width-1-2@m">
        <div className="header">
          <p className="title">{this.props.location.name}</p>
          <div className="icon">
            <img src={`assets/img/${this.props.location.type}.svg`} alt={`${this.props.location.type}`} />
          </div>
        </div>
        <div className="value">
          {this.props.location.cost} {this.props.location.currency}
        </div>
        <div className="footer">
          {this.displayRevenue()}
          <img
            alt="info"
            className="info"
            src="/assets/img/info.svg"
            uk-tooltip={tooltipSettings}
          />
        </div>
      </div>
    );
  }
}
