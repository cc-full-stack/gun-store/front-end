import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import LocationsConnected from './Locations';
import CartConnected from '../shop/Cart';

export class Locations extends Component {
  constructor(props) {
    super(props);
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange() {
    this.props.history.push('/slider');
  }

  render() {
    return (
      <div className="uk-container uk-padding">
        <nav uk-navbar>
          <div className="uk-navbar-right">
            <ul className="uk-navbar-nav">
              <li className="uk-active">
                <button
                  className="uk-button uk-button-default"
                  uk-toggle="target: #cart-off-canvas"
                >
                  <span uk-icon="icon: cart" />
                </button>
              </li>
              <li className="uk-active">
                <button
                  onClick={this.routeChange}
                  id="router-btn"
                  className="uk-button uk-button-default"
                >
                  HOME &nbsp;
                </button>
              </li>
            </ul>
          </div>
        </nav>
        <LocationsConnected />
        <CartConnected />
      </div>
    );
  }
}

export default withRouter(Locations);
