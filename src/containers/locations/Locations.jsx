import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import CardConnected from './Card';
import { locationSetItems } from '../../actions/storeActions';
import { StyledMapWithAnInfoBox } from '../map/map';

import './Locations.scss';

export class Locations extends Component {
  constructor(props) {
    super(props);
    this.loading = true;
    this.fetchLocations();
  }

  fetchLocations() {
    return axios
      .get('/locations')
      .then((response) => {
        this.loading = false;
        return this.props.setLocations(response.data);
      })
      .catch((error) => {
        /* eslint-disable no-undef */
        UIkit.notification(
          { message: `<span uk-icon='icon: close'></span>${error.message}`, status: 'danger' },
          {
            pos: 'top-right',
          },
        );
        /* eslint-enable */
      });
  }

  render() {
    if (this.loading) {
      return (
        <div
          className="products uk-child-width-1-3@m uk-grid-match uk-margin-medium"
          uk-lightbox="animation: slide"
          uk-grid="true"
        >
          <div className="uk-flex uk-flex-column" />
          <div className="uk-flex uk-flex-column uk-flex-center">
            <span data-uk-spinner="" className="spin uk-flex" uk-spinner="ratio: 5" />
          </div>
          <div className="uk-flex uk-flex-column" />
        </div>
      );
    }
    return (
      <div className="flex">
        <div
          className="locations uk-margin-medium"
          uk-lightbox="animation: slide"
        >
          {this.props.locations.map(location => (
            <CardConnected key={location.id} location={location} />
          ))}
        </div>
        <div className="map">
          <StyledMapWithAnInfoBox
            mapStyle="secondary"
            center={{ lat: 44.5, lng: 23.0 }}
            locations={this.props.locations}
          />
        </div>
      </div>
    );
  }
}

export const storeLocationsToProps = state => ({
  locations: state.reducers.store.locations,
});

export const storeDispatchToProps = dispatch => ({
  setLocations: locations => dispatch(locationSetItems(locations)),
});

export default connect(storeLocationsToProps, storeDispatchToProps)(Locations);
