import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from './reducers';

/* eslint-disable no-underscore-dangle */
// Setup inspection for ReduxDevTools extension
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

export default createStore(
  combineReducers({ reducers }),
  {},
  composeEnhancers(applyMiddleware(logger, thunk)),
);
