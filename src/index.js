// polyfill for older browsers
import 'babel-polyfill';
// React / Redux
import React from 'react';
import axios from 'axios';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './containers/App';

// create application store
import store from './store';

// Set baseurl for axios
axios.defaults.baseURL = '//gun-store-back-end.herokuapp.com';

// render application
/* eslint-disable react/jsx-filename-extension */
const AppWithProvider = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

/* istanbul ignore next */
if (process.env.NODE_ENV !== 'test') {
  render(<AppWithProvider />, document.getElementById('root'));
}
/* eslint-enable */

export default AppWithProvider;
