import * as actionTypes from './actionTypes';

/**
 * add product to cart
 * @return {dispatch} to trigger reducer
 */
export function cartAddProduct(product) {
  return { type: actionTypes.CART_ADD_PRODUCT, payload: product };
}

/**
 * remove product from cart
 * @return {dispatch} to trigger reducer
 */
export function cartRemoveProduct(product) {
  return { type: actionTypes.CART_REMOVE_PRODUCT, payload: product };
}

/**
 * add product to store
 * @return {dispatch} to trigger reducer
 */
export function storeAddProduct(product) {
  return { type: actionTypes.STORE_ADD_PRODUCT, payload: product };
}

/**
 * set product qty
 * @return {dispatch} to trigger reducer
 */
export function cartSetProductQTY(data) {
  return { type: actionTypes.CART_SET_PRODUCT_QTY, payload: data };
}

/**
 * set products in store
 * @return {dispatch} to trigger reducer
 */
export function storeSetProducts(data) {
  return { type: actionTypes.STORE_SET_PRODUCTS, payload: data };
}

/**
 * add items in location
 * @return {dispatch} to trigger reducer
 */
export function locationAddItems(data) {
  return { type: actionTypes.LOCATION_ADD_ITEMS, payload: data };
}


/**
 * set items in location
 * @return {dispatch} to trigger reducer
 */
export function locationSetItems(data) {
  return { type: actionTypes.LOCATION_SET_ITEMS, payload: data };
}
