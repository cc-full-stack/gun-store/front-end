import * as actionTypes from '../actions/actionTypes';

export const initialStoreState = {
  cart: [],
  products: [],
  locations: [],
};

export class CartProduct {
  constructor(product) {
    this.id = product.id;
    this.title = product.title;
    this.price = product.price;
    this.image = product.image;
    this.qty = 1;
  }
}

const store = (state = initialStoreState, action) => {
  // defined reducer actions to manipulate the store
  switch (action.type) {
    // Add product to cart
    case actionTypes.CART_ADD_PRODUCT:
      if (state.cart.map(product => product.id).includes(action.payload.id)) {
        state = {
          ...state,
          cart: state.cart.map(product =>
            (product.id !== action.payload.id ? product : { ...product, qty: product.qty + 1 })),
        };
      } else {
        state = {
          ...state,
          cart: state.cart.concat(new CartProduct(action.payload)),
        };
      }
      break;
    // Remove product from cart
    case actionTypes.CART_REMOVE_PRODUCT:
      state = {
        ...state,
        cart: state.cart.filter(product => product.id !== action.payload.id),
      };
      break;
    // Change product quantity in cart
    case actionTypes.CART_SET_PRODUCT_QTY:
      if (state.cart.map(product => product.id).includes(action.payload.id)) {
        state = {
          ...state,
          cart: state.cart.map(product =>
            (product.id !== action.payload.id ? product : { ...product, qty: action.payload.qty })),
        };
      }
      break;
    // Change product quantity in cart
    case actionTypes.STORE_SET_PRODUCTS:
      state = {
        ...state,
        products: action.payload,
      };
      break;
    // Set locations in store
    case actionTypes.LOCATION_SET_ITEMS:
      state = {
        ...state,
        locations: action.payload,
      };
      break;
    // Add product in store
    case actionTypes.STORE_ADD_PRODUCT:
      state = {
        ...state,
        products: state.products.concat(action.payload),
      };
      break;
    // handle default
    default:
      return state;
  }
  return state;
};

export default store;
