import store, { CartProduct, initialStoreState } from '../../reducers/store';
import * as actionTypes from '../../actions/actionTypes';

const product = {
  id: 1,
  title: 'some',
  price: 300,
  image: 'some.png',
};

const location = {
  name: 'Test Store',
  revenue: 33,
  cost: 234432,
  currency: '$',
  type: 'store',
  description: 'This is a dummy text it will be replaced with the real one in production',
  coordinates: {
    lat: 44.3,
    lon: 22.2,
  },
};

/* eslint-env jest */

describe('cart product class', () => {
  it('should return correct instance', () => {
    const instance = new CartProduct(product);
    expect(instance).toHaveProperty('id');
    expect(instance).toHaveProperty('title');
    expect(instance).toHaveProperty('price');
    expect(instance).toHaveProperty('image');
    expect(instance).toHaveProperty('qty');
    expect(instance.id).toEqual(product.id);
    expect(instance.title).toEqual(product.title);
    expect(instance.price).toEqual(product.price);
    expect(instance.image).toEqual(product.image);
    expect(instance.qty).toEqual(1);
  });
});

describe('store reducer', () => {
  it('should return the initial state', () => {
    expect(store(initialStoreState, {
      type: 'unexistent',
      payload: null,
    })).toEqual(initialStoreState);
  });

  it('should return the initial state with undefined initialState provided', () => {
    expect(store(undefined, {
      type: 'unexistent',
      payload: null,
    })).toEqual(initialStoreState);
  });

  it('should handle CART_ADD_PRODUCT', () => {
    expect(store(initialStoreState, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    }).cart).toContainEqual(new CartProduct(product));
  });

  it('should not add product twice to cart', () => {
    let stateWithProducts = store(initialStoreState, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    });
    expect(stateWithProducts.cart).toContainEqual(new CartProduct(product));
    stateWithProducts = store(stateWithProducts, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    });
    expect(store(stateWithProducts, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    }).cart).toHaveLength(1);
    expect(stateWithProducts.cart[0]).toHaveProperty('qty');
    expect(stateWithProducts.cart[0].qty).toEqual(2);
  });

  it('should increase product quantity with multiple poducts in cart', () => {
    const newProduct = {
      id: 2,
      title: 'some',
      price: 300,
      image: 'some.png',
    };
    let stateWithProducts = store(initialStoreState, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    });
    stateWithProducts = store(stateWithProducts, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: newProduct,
    });
    stateWithProducts = store(stateWithProducts, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: newProduct,
    });
    expect(stateWithProducts.cart).toHaveLength(2);
    expect(stateWithProducts.cart[1]).toHaveProperty('qty');
    expect(stateWithProducts.cart[1].qty).toEqual(2);
  });

  it('should handle CART_REMOVE_PRODUCT', () => {
    const stateWithProduct = store(initialStoreState, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    });
    expect(stateWithProduct.cart).toContainEqual(new CartProduct(product));
    expect(store(stateWithProduct, {
      type: actionTypes.CART_REMOVE_PRODUCT,
      payload: product,
    }).cart).toHaveLength(0);
  });

  it('should handle CART_SET_PRODUCT_QTY', () => {
    let stateWithProducts = store(initialStoreState, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    });
    const instance = {
      id: product.id,
      qty: 3,
    };
    expect(stateWithProducts.cart).toContainEqual(new CartProduct(product));
    stateWithProducts = store(stateWithProducts, {
      type: actionTypes.CART_SET_PRODUCT_QTY,
      payload: instance,
    });
    expect(stateWithProducts.cart).toHaveLength(1);
    expect(stateWithProducts.cart[0]).toHaveProperty('qty');
    expect(stateWithProducts.cart[0].qty).toEqual(3);
  });

  it('should set product quantity with multiple products in cart', () => {
    const newProduct = {
      id: 2,
      title: 'some',
      price: 300,
      image: 'some.png',
    };
    let stateWithProducts = store(initialStoreState, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: product,
    });
    stateWithProducts = store(stateWithProducts, {
      type: actionTypes.CART_ADD_PRODUCT,
      payload: newProduct,
    });
    const instance = {
      id: product.id,
      qty: 3,
    };
    stateWithProducts = store(stateWithProducts, {
      type: actionTypes.CART_SET_PRODUCT_QTY,
      payload: instance,
    });
    expect(stateWithProducts.cart).toHaveLength(2);
    expect(stateWithProducts.cart[0]).toHaveProperty('qty');
    expect(stateWithProducts.cart[0].qty).toEqual(3);
  });

  it('should handle CART_SET_PRODUCT_QTY with wrong product id', () => {
    const instance = {
      id: product.id,
      qty: 3,
    };
    const stateWithSetQty = store(initialStoreState, {
      type: actionTypes.CART_SET_PRODUCT_QTY,
      payload: instance,
    });
    expect(stateWithSetQty).toEqual(initialStoreState);
  });

  it('should handle STORE_SET_PRODUCTS', () => {
    expect(store(initialStoreState, {
      type: actionTypes.STORE_SET_PRODUCTS,
      payload: [product],
    }).products).toEqual([product]);
  });

  it('should handle STORE_ADD_PRODUCT', () => {
    expect(store(initialStoreState, {
      type: actionTypes.STORE_ADD_PRODUCT,
      payload: product,
    }).products).toEqual([product]);
  });

  it('should handle LOCATION_SET_ITEMS', () => {
    expect(store(initialStoreState, {
      type: actionTypes.LOCATION_SET_ITEMS,
      payload: [location],
    }).locations).toEqual([location]);
  });
});
