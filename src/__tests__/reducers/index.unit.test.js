import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../../reducers';

/* eslint-env jest */

describe('app reducers', () => {
  it('should create default state structure', () => {
    const store = createStore(combineReducers({ reducers }), {}, applyMiddleware(thunk));

    expect(store.getState()).toHaveProperty('reducers');
    expect(store.getState().reducers).toHaveProperty('store');
  });
});
