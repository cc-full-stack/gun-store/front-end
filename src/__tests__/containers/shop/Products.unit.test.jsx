import React from 'react';
import axios from 'axios';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';
import MockAdapter from 'axios-mock-adapter';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import * as component from '../../../containers/shop/Products';
import { product } from '../../actions/storeActions.unit.test';
import reducers from '../../../reducers';
import { storeSetProducts } from '../../../actions/storeActions';

let wrapper;
const notification = jest.fn();
global.UIkit = { notification };

/* eslint-env jest */
const store = createStore(combineReducers({ reducers }), {}, applyMiddleware(thunk));
const dispatch = jest.spyOn(store, 'dispatch');

beforeEach(() => {
  wrapper = shallow(<component.Products
    {...{
      ...component.storeDispatchToProps(store.dispatch),
      ...component.storeProductsToProps(store.getState()),
    }}
  />);
});

describe('Products container', () => {
  it('should show loader spin on loading server data', async () => {
    const spin = wrapper.find('.spin');

    const data = [product];
    const mock = new MockAdapter(axios, { delayResponse: 2000 });
    mock.onGet('/products').reply(200, data);

    expect(spin).not.toBeNull();
  });

  it('should load server data', async () => {
    const data = [product];
    const mock = new MockAdapter(axios);
    mock.onGet('/products').reply(200, data);

    await wrapper.instance().fetchProducts();
    wrapper = shallow(<component.Products
      {...{
        ...component.storeDispatchToProps(store.dispatch),
        ...component.storeProductsToProps(store.getState()),
      }}
    />);
    await wrapper.instance().fetchProducts();

    expect(wrapper.instance().loading).toBeFalsy();
    expect(dispatch).toBeCalledWith(storeSetProducts([product]));
    expect(wrapper.instance().props.products.length).toEqual(1);
  });

  it('should render products in container', async () => {
    const data = [product];
    const mock = new MockAdapter(axios);
    mock.onGet('/products').reply(200, data);

    await wrapper.instance().fetchProducts();
    wrapper = shallow(<component.Products
      {...{
        ...component.storeDispatchToProps(store.dispatch),
        ...component.storeProductsToProps(store.getState()),
      }}
    />);
    await wrapper.instance().fetchProducts();
    wrapper.rerender();
    const products = wrapper.find('.add_card');

    expect(products).not.toBeNull();
  });

  it('should show notification when getting error', async () => {
    const data = { message: 'fail' };

    const mock = new MockAdapter(axios);
    mock.onGet('/products').reply(400, data);

    await wrapper.instance().fetchProducts();

    expect(notification).toHaveBeenCalled();
  });
});
