import React from 'react';
import { shallow } from 'enzyme';
import { CartProduct, cartDispatchToProps } from '../../../containers/shop/CartProduct';
import { CartProduct as CartProductClass } from '../../../reducers/store';
import { cartRemoveProduct, cartSetProductQTY } from '../../../actions/storeActions';
import { product as prod } from '../../actions/storeActions.unit.test';

/* eslint-env jest */

const product = new CartProductClass(prod);

let wrapper;
const dispatch = jest.fn();
const changeQty = jest.spyOn(CartProduct.prototype, 'handleProductQtyChange');
const productRemoveHandler = jest.spyOn(CartProduct.prototype, 'handleCartProductDelete');

beforeEach(() => {
  wrapper = shallow(<CartProduct {...{ ...cartDispatchToProps(dispatch), product }} />);
});

describe('The cart item module', () => {
  describe('the name display of the item', () => {
    it('should display name of the item', () => {
      const label = wrapper.find('span.uk-link-reset');

      expect(label.text()).toEqual(product.title);
    });
  });

  describe('the photo of the item', () => {
    it('should set src and alt for img tag', () => {
      const imgTag = wrapper.find('img.uk-comment-avatar');

      expect(imgTag.prop('src')).toEqual(product.image);
      expect(imgTag.prop('alt')).toEqual(product.title);
    });
  });

  describe('the qty container of the item', () => {
    it('should contain price of item', () => {
      const container = wrapper.find('li > span');

      expect(container.text()).toContain(product.price);
    });
  });

  describe('the qty input of the item', () => {
    it('should contain quantity of item', () => {
      const input = wrapper.find('input[name="qty"]');
      expect(input.prop('value')).toEqual(product.qty);
    });

    it('should handle quantity change of item', () => {
      const event = { target: { value: 1 } };
      const input = wrapper.find('input[name="qty"]');
      input.simulate('change', event);

      expect(changeQty).toBeCalledWith(event);
      expect(dispatch).toBeCalledWith(cartSetProductQTY({
        id: product.id,
        qty: event.target.value,
      }));
    });
  });

  describe('Delete from cart button', () => {
    it('should remove item from cart', () => {
      const button = wrapper.find('button');
      button.simulate('click');

      expect(productRemoveHandler).toBeCalled();
      expect(dispatch).toBeCalledWith(cartRemoveProduct(product));
    });
  });
});
