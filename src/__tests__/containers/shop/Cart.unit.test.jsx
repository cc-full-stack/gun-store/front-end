import React from 'react';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Cart, cartProductsToProps } from '../../../containers/shop/Cart';
import { product } from '../../actions/storeActions.unit.test';
import reducers from '../../../reducers';
import { cartAddProduct } from '../../../actions/storeActions';

let wrapper;

const store = createStore(combineReducers({ reducers }), {}, applyMiddleware(thunk));

/* eslint-env jest */

beforeEach(() => {
  wrapper = shallow(<Cart {...{ ...cartProductsToProps(store.getState()) }} />);
});

describe('Cart container', () => {
  it('should empty cart message', () => {
    const msg = wrapper.find('.cart__message');
    expect(msg.text()).toContain('There is no items in cart');
  });

  it('should calculate total when cart is not empty', () => {
    store.dispatch(cartAddProduct(product));
    wrapper = shallow(<Cart {...{ ...cartProductsToProps(store.getState()) }} />);
    const total = wrapper.find('h6');
    expect(total.text()).toContain(product.price);
  });
});
