import React from 'react';
import { shallow } from 'enzyme';
import { Shop } from '../../../containers/shop';

let wrapper;
const push = jest.fn();

/* eslint-env jest */

beforeEach(() => {
  wrapper = shallow(<Shop {...{ history: { push } }} />);
});

describe('Shop container', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should handle route change', () => {
    const button = wrapper.find('#router-btn');
    button.simulate('click');

    expect(push).toBeCalled();
  });
});
