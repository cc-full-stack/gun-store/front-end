import React from 'react';
import { shallow } from 'enzyme';
import { Product, cartDispatchToProps } from '../../../containers/shop/Product';
import { product } from '../../actions/storeActions.unit.test';
import { cartAddProduct } from '../../../actions/storeActions';

let wrapper;
const dispatch = jest.fn();
const notification = jest.fn();
const handleAddToCart = jest.spyOn(Product.prototype, 'handleAddToCart');
global.UIkit = { notification };

/* eslint-env jest */

beforeEach(() => {
  wrapper = shallow(<Product {...{ ...cartDispatchToProps(dispatch), product }} />);
});

describe('Product container', () => {
  it('should render component', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render product title', () => {
    const title = wrapper.find('.uk-card-title');

    expect(title.text()).toEqual(product.title);
  });

  it('should render product media', () => {
    const media = wrapper.find('.product__media');

    expect(media.prop('href')).toEqual(product.image);
    expect(media.prop('caption')).toEqual(product.title);
  });

  it('should render product price', () => {
    const price = wrapper.find('.product__price');

    expect(price.text()).toContain(product.price);
  });

  it('should dispatch addToCartProduct action on button click', () => {
    const button = wrapper.find('button');
    button.simulate('click');

    expect(handleAddToCart).toBeCalled();
    expect(dispatch).toBeCalledWith(cartAddProduct(product));
    expect(notification).toBeCalled();
  });
});
