import React from 'react';
import { shallow } from 'enzyme';
import App from '../../containers/App';

/* eslint-env jest */

describe('App container', () => {
  it('should render component', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });
});
