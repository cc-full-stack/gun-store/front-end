import React from 'react';
import { shallow } from 'enzyme';
import { MapComponent, initialState, onToggleOpen } from '../../../containers/map/map';
import { location } from '../../actions/storeActions.unit.test';

/* eslint-env jest */

const center = { lat: 47.0105, lng: 28.8638 };
const mapStyle = 'default';
const isOpen = true;

describe('Map component', () => {
  it('should render', () => {
    const wrapper = shallow(<MapComponent {...{ center }} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render info box on state change', () => {
    const wrapper = shallow(<MapComponent {...{ isOpen }} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render location pins', () => {
    const wrapper = shallow(<MapComponent {...{ key: location.id, locations: [location] }} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should display certain theme', () => {
    const wrapper = shallow(<MapComponent {...{ center, mapStyle }} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should display default location', () => {
    const wrapper = shallow(<MapComponent {...{
      defaultLocation: true,
      center,
      mapStyle,
      isOpen,
    }}
    />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should toggle tooltip', () => {
    let state = initialState;
    expect(state.isOpen).not.toBeFalsy();

    state = onToggleOpen(initialState);

    expect(state().isOpen).toBeFalsy();
  });
});
