import React from 'react';
import { shallow } from 'enzyme';
import { Locations } from '../../../containers/locations';

let wrapper;
const push = jest.fn();

/* eslint-env jest */

beforeEach(() => {
  wrapper = shallow(<Locations {...{ history: { push } }} />);
});

describe('Locations container', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should handle route change', () => {
    const button = wrapper.find('#router-btn');
    button.simulate('click');

    expect(push).toBeCalled();
  });
});
