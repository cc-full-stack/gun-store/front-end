import React from 'react';
import { shallow } from 'enzyme';
import Card from '../../../containers/locations/Card';
import { location } from '../../actions/storeActions.unit.test';

let wrapper;

/* eslint-env jest */

beforeEach(() => {
  wrapper = shallow(<Card key={location.id} location={location} />);
});

describe('Card container', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should show different image depending on revenue', () => {
    const loc = { ...location };
    loc.revenue = -22;
    wrapper = shallow(<Card location={loc} />);
    expect(wrapper).toMatchSnapshot();
  });
});
