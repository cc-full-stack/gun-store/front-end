import React from 'react';
import axios from 'axios';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';
import MockAdapter from 'axios-mock-adapter';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { location } from '../../actions/storeActions.unit.test';
import reducers from '../../../reducers';
import * as component from '../../../containers/locations/Locations';
import { locationSetItems } from '../../../actions/storeActions';

let wrapper;
const push = jest.fn();
const notification = jest.fn();
global.UIkit = { notification };

/* eslint-env jest */
const store = createStore(combineReducers({ reducers }), {}, applyMiddleware(thunk));
const dispatch = jest.spyOn(store, 'dispatch');

beforeEach(() => {
  wrapper = shallow(<component.Locations {...{ history: { push } }} />);
});

describe('Card container', () => {
  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should show loader spin on loading server data', async () => {
    const spin = wrapper.find('.spin');

    const data = [location];
    const mock = new MockAdapter(axios, { delayResponse: 2000 });
    mock.onGet('/locations').reply(200, data);

    expect(spin).not.toBeNull();
  });

  it('should load server data', async () => {
    const data = [location];
    const mock = new MockAdapter(axios);
    mock.onGet('/locations').reply(200, data);

    await wrapper.instance().fetchLocations();
    wrapper = shallow(<component.Locations
      {...{
        ...component.storeDispatchToProps(store.dispatch),
        ...component.storeLocationsToProps(store.getState()),
      }}
    />);
    await wrapper.instance().fetchLocations();

    expect(wrapper.instance().loading).toBeFalsy();
    expect(dispatch).toBeCalledWith(locationSetItems([location]));
    expect(wrapper.instance().props.locations.length).not.toBeNull();
  });

  it('should render locations in container', async () => {
    const data = [location];
    const mock = new MockAdapter(axios);
    mock.onGet('/locations').reply(200, data);

    await wrapper.instance().fetchLocations();
    wrapper = shallow(<component.Locations
      {...{
        ...component.storeDispatchToProps(store.dispatch),
        ...component.storeLocationsToProps(store.getState()),
      }}
    />);
    await wrapper.instance().fetchLocations();
    wrapper.rerender();
    const locations = wrapper.find('.flex');

    expect(locations).not.toBeNull();
  });

  it('should show notification when getting error', async () => {
    const data = { message: 'fail' };

    const mock = new MockAdapter(axios);
    mock.onGet('/locations').reply(400, data);

    await wrapper.instance().fetchLocations();

    expect(notification).toHaveBeenCalled();
  });
});
