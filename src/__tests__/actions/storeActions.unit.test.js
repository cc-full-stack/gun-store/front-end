import * as actions from '../../actions/storeActions';
import * as actionTypes from '../../actions/actionTypes';

export const product = {
  id: '5aaa332ec214f525d9ff771b',
  title: 'Ak 47',
  image: 'http://www.thespecialistsltd.com/sites/default/files/Replica_AK47.jpg',
  price: 2000,
};

export const location = {
  id: '44.322.2',
  name: 'Test Store',
  revenue: 33,
  cost: 234432,
  currency: '$',
  type: 'store',
  description: 'This is a dummy text it will be replaced with the real one in production',
  coordinates: {
    lat: 47.0105,
    lon: 28.8638,
  },
};

/* eslint-env jest */

describe('ACIONS === Store actions', () => {
  it('should add product to cart', () => {
    const add = actions.cartAddProduct(product);
    expect(add).toEqual({ type: actionTypes.CART_ADD_PRODUCT, payload: product });
  });

  it('should set product qty on cart', () => {
    const add = actions.cartSetProductQTY(product);
    expect(add).toEqual({ type: actionTypes.CART_SET_PRODUCT_QTY, payload: product });
  });

  it('should remove product from cart', () => {
    const add = actions.cartRemoveProduct(product);
    expect(add).toEqual({ type: actionTypes.CART_REMOVE_PRODUCT, payload: product });
  });

  it('should add product to store', () => {
    const add = actions.storeAddProduct(product);
    expect(add).toEqual({ type: actionTypes.STORE_ADD_PRODUCT, payload: product });
  });

  it('should set locations in store', () => {
    const add = actions.locationSetItems(location);
    expect(add).toEqual({ type: actionTypes.LOCATION_SET_ITEMS, payload: location });
  });

  it('should add locations in store', () => {
    const add = actions.locationAddItems(location);
    expect(add).toEqual({ type: actionTypes.LOCATION_ADD_ITEMS, payload: location });
  });
});
