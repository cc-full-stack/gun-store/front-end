import React from 'react';
import { shallow } from 'enzyme';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { AddProductModal, storeDispatchToProps } from '../../components/AddProductModal';
import { product } from '../actions/storeActions.unit.test';
import { storeAddProduct } from '../../actions/storeActions';

const hide = jest.fn();
const dispatch = jest.fn();
const notification = jest.fn();
global.UIkit = { modal: () => ({ hide }), notification };
let wrapper;
const handleTitleChange = jest.spyOn(AddProductModal.prototype, 'handleTitleChange');
const handleImageChange = jest.spyOn(AddProductModal.prototype, 'handleImageChange');
const handlePriceChange = jest.spyOn(AddProductModal.prototype, 'handlePriceChange');
const handleFormSubmit = jest.spyOn(AddProductModal.prototype, 'handleFormSubmit');

/* eslint-env jest */

beforeEach(() => {
  wrapper = shallow(<AddProductModal {...{ ...storeDispatchToProps(dispatch) }} />);
});

describe('AddProductModal component', () => {
  it('should render component', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render component', () => {
    const instance = wrapper.instance();
    instance.setState({ title: 'some', image: 'else', price: 123 });
    expect(instance.isValid()).toBeTruthy();
  });

  it('should handle title change', () => {
    const event = { target: { value: 'TT45' } };
    const input = wrapper.find('#product-title');
    input.simulate('change', event);
    expect(handleTitleChange).toHaveBeenCalledWith(event);
  });

  it('should handle image change', () => {
    const event = { target: { value: 'TT45' } };
    const input = wrapper.find('#product-image');
    input.simulate('change', event);
    expect(handleImageChange).toHaveBeenCalledWith(event);
  });

  it('should handle price change', () => {
    const event = { target: { value: 'TT45' } };
    const input = wrapper.find('#product-price');
    input.simulate('change', event);
    expect(handlePriceChange).toHaveBeenCalledWith(event);
  });

  it('should handle add form submit', () => {
    const input = wrapper.find('form');
    input.simulate('submit', { preventDefault() {} });

    expect(handleFormSubmit).toHaveBeenCalled();
  });

  it('should dispatch addProduct action and handle success response', async () => {
    const data = { message: 'success' };
    dispatch.mockReset();

    const mock = new MockAdapter(axios);
    mock.onPost('/products').reply(200, data);
    wrapper.instance().setState({ ...product });

    await wrapper.instance().handleFormSubmit({ preventDefault() {} });

    expect(dispatch).toBeCalledWith(storeAddProduct(product));
    expect(notification).toBeCalled();
    expect(hide).toBeCalled();
  });

  it('should dispatch addProduct action and handle fail response', async () => {
    const data = { message: 'fail' };
    dispatch.mockReset();

    const mock = new MockAdapter(axios);
    mock.onPost('/products').reply(400, data);
    wrapper.instance().setState({ ...product });

    await wrapper.instance().handleFormSubmit({ preventDefault() {} });

    expect(dispatch).not.toHaveBeenCalled();
    expect(notification).toBeCalled();
    expect(hide).toBeCalled();
  });
});
