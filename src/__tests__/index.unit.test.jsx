import axios from 'axios';
import React from 'react';
import { shallow } from 'enzyme';
import AppWithProvider from '../';

/* eslint-env jest */

describe('API endpoint', () => {
  it('should be setted', () => {
    expect(axios.defaults.baseURL).not.toHaveLength(0);
  });
});

describe('Root component', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<AppWithProvider />);
  });

  it('should render', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should have state', () => {
    wrapper = shallow(<AppWithProvider />).dive();

    expect(wrapper.instance()).toHaveProperty('state');
  });

  it('should have state', () => {
    wrapper = shallow(<AppWithProvider />).dive();

    expect(wrapper.instance()).toHaveProperty('state');
  });
});
