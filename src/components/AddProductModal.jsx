import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { storeAddProduct } from '../actions/storeActions';

export class AddProductModal extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      title: '',
      image: '',
      price: 0,
    };
    this.state = { ...this.initialState };
    this.validators = {
      title: () => this.state.title.length !== 0,
      image: () => this.state.image.length !== 0,
      price: () => this.state.price !== 0,
    };
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }

  isValid() {
    return Object.keys(this.validators)
      .map(key => this.validators[key]())
      .reduce((a, b) => a && b);
  }

  handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  handleImageChange(e) {
    this.setState({ image: e.target.value });
  }

  handlePriceChange(e) {
    this.setState({
      price: parseInt(e.target.value, 10) || 0,
    });
  }

  handleFormSubmit(e) {
    e.preventDefault();
    /* eslint-disable no-undef */
    UIkit.modal(document.getElementById('add-product-modal')).hide();
    /* eslint-enable */
    const product = { ...this.state };
    this.setState(this.initialState);
    return this.props.addProduct(product);
  }

  render() {
    return (
      <div id="add-product-modal" uk-modal>
        <div className="uk-modal-dialog uk-modal-body">
          <form onSubmit={this.handleFormSubmit}>
            <fieldset className="uk-fieldset">
              <legend className="uk-legend">Add new product</legend>

              <div className="uk-margin">
                <label className="uk-form-label" htmlFor="product-title">
                  Product title
                  <div className="uk-form-controls">
                    <input
                      className={`uk-input ${
                        !this.validators.title() ? 'uk-form-danger' : 'uk-form-success'
                      }`}
                      id="product-title"
                      type="text"
                      placeholder="title here..."
                      value={this.state.title}
                      onChange={this.handleTitleChange}
                    />
                  </div>
                </label>
              </div>

              <div className="uk-margin">
                <label className="uk-form-label" htmlFor="product-price">
                  Product price
                  <div className="uk-form-controls">
                    <input
                      className={`uk-input ${
                        !this.validators.price() ? 'uk-form-danger' : 'uk-form-success'
                      }`}
                      id="product-price"
                      type="number"
                      min="0"
                      value={this.state.price}
                      onChange={this.handlePriceChange}
                    />
                  </div>
                </label>
              </div>

              <div className="uk-margin">
                <label className="uk-form-label" htmlFor="product-image">
                  Product image
                  <div className="uk-form-controls">
                    <input
                      className={`uk-input ${
                        !this.validators.image() ? 'uk-form-danger' : 'uk-form-success'
                      }`}
                      id="product-image"
                      type="text"
                      value={this.state.image}
                      onChange={this.handleImageChange}
                    />
                  </div>
                </label>
              </div>

              <div className="uk-margin">
                <button
                  className="uk-button uk-button-default uk-margin-small-right"
                  type="submit"
                  disabled={this.isValid() ? null : true}
                >
                  Add
                </button>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

export const storeDispatchToProps = dispatch => ({
  addProduct: product =>
    axios
      .post('/products', product)
      .then((respose) => {
        /* eslint-disable no-undef */
        UIkit.notification(`<span uk-icon='icon: check'></span> ${respose.data.message}.`, {
          pos: 'top-right',
        });
        /* eslint-enable */
        return dispatch(storeAddProduct(product));
      })
      .catch((err) => {
        /* eslint-disable no-undef */
        UIkit.notification(`<span uk-icon='icon: check'></span> ${err}`, {
          pos: 'top-right',
        });
        /* eslint-enable */
      }),
});

export default connect(null, storeDispatchToProps)(AddProductModal);
